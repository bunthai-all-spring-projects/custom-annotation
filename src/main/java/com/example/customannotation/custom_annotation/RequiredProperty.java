package com.example.customannotation.custom_annotation;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONObject;

import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.security.InvalidParameterException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Project custom-annotation
 * Author bunthai.den
 * Date 5/28/2022
 */
@Documented
@Constraint(validatedBy = RequiredPropertyValidator.class)
@Target({ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
public @interface RequiredProperty {
    String message() default "Required property: %s";

    String[] parameters() default {};

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}


class RequiredPropertyValidator implements ConstraintValidator<RequiredProperty, Object> {

    private RequiredProperty requiredProperty;

    @Override
    public void initialize(RequiredProperty constraintAnnotation) {
        this.requiredProperty = constraintAnnotation;
    }

    @Override
    public boolean isValid(Object requestBody, ConstraintValidatorContext var2) {
        List<String> parameters = Arrays.asList(requiredProperty.parameters());
        String message = requiredProperty.message();

        System.out.println(requestBody);
        List<String> notFoundParamters = notFoundParamters = parameters.stream().filter(s ->
                {
                    if (requestBody instanceof Map) {
                        Map map = new ObjectMapper().convertValue(requestBody, Map.class);
                        return !map.containsKey(s);
                    }
                    /* Spring no long use JSONObject as request body */
//                    else if (requestBody instanceof JSONObject) {
//                        JSONObject obj = new JSONObject(requestBody);
//                        return !obj.has(s);
//                    }
                    return false;
                }
        ).collect(Collectors.toList());

        if (!notFoundParamters.isEmpty()) {
            throw new IllegalArgumentException(String.format(message, notFoundParamters));
        }
        return true;
    }

}

