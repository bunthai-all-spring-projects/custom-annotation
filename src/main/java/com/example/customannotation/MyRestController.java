package com.example.customannotation;

import com.example.customannotation.custom_annotation.RequiredProperty;
import org.json.JSONObject;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;


/**
 * Project custom-annotation
 * Author bunthai.den
 * Date 5/28/2022
 */
@RestController
@Validated
public class MyRestController {

    @PostMapping
    public String hello(
            @RequestBody
            @RequiredProperty(parameters = {"id", "name", "gender"}) Map<String, Object> requestBody
    ) {
        return "hello";
    }


}
